package si.uni_lj.fri.pbd.miniapp1;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ContactHolder> {

    ArrayList<Contact> allContacts;

    public ContactsAdapter(ArrayList<Contact> allContacts) {
        this.allContacts = allContacts;
    }

    @NonNull
    @Override
    public ContactHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ContactHolder viewHolder = new ContactHolder(LayoutInflater.from(parent.getContext())
                                    .inflate(R.layout.contacts_item_list, parent, false));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ContactHolder holder, int position) {
        final Contact contact = allContacts.get(position);
        CheckBox checkBox = holder.checkBox;
        checkBox.setChecked(contact.isChecked());
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                contact.setChecked(isChecked);
            }
        });
        checkBox.setText(contact.getName());
    }

    @Override
    public int getItemCount() {
        if(allContacts == null)
            return 0;
        return allContacts.size();
    }


    // for every single UI element
    public class ContactHolder extends RecyclerView.ViewHolder {
        CheckBox checkBox;

        public ContactHolder(@NonNull View itemView) {
            super(itemView);
            checkBox = (CheckBox) itemView;
        }
    }
}
