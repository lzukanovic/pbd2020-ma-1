package si.uni_lj.fri.pbd.miniapp1;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ViewFlipper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

// TODO fix no permission message when in landscape

public class ContactsFragment extends Fragment {

    private ViewFlipper viewFlipper;
    private RecyclerView contactsList;
    private ArrayList<Contact> allContacts;
    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_contacts, container, false);

        contactsList = view.findViewById(R.id.contact_list);
        contactsList.setLayoutManager(new LinearLayoutManager(getActivity()));

        viewFlipper = view.findViewById(R.id.view_flipper);

        return view;
    }

    // method requests data from MainActivity and passes it on to the RecyclerView
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // reqest contacts from main activity
        MainActivity mainActivity = (MainActivity) getActivity();
        allContacts = mainActivity.returnContacts();

        if(allContacts != null && allContacts.size() > 0) {
            ContactsAdapter contactsAdapter = new ContactsAdapter(allContacts);
            contactsList.setAdapter(contactsAdapter);
            contactsList.addItemDecoration(
                    new DividerItemDecoration(contactsList.getContext(), DividerItemDecoration.VERTICAL));
        } else {
            viewFlipper.setDisplayedChild(1);
        }
    }
}
