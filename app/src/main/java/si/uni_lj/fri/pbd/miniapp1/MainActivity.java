package si.uni_lj.fri.pbd.miniapp1;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.material.navigation.NavigationView;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    // TODO improve nav header styling

    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;

    private DrawerLayout mDrawer;
    private Toolbar toolbar;
    private NavigationView navDrawer;
    private ActionBarDrawerToggle drawerToggle;
    private ArrayList<Contact> allContacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // open the default fragment
        HomeFragment homeFragment = new HomeFragment();
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.fragment_content, homeFragment).commit();

        // set default title
        setTitle(R.string.drawer_item1);

        // replace ActionBar with toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // add button to toolbar to bring up navigation drawer
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // find and set drawer layout
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        // setup toggle to display hamburger icon
        drawerToggle = setupDrawerToggle();
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerToggle.syncState();
        // link DrawerLayout events to ActionBarToggle
        mDrawer.addDrawerListener(drawerToggle);

        // find and set drawer view
        navDrawer = (NavigationView) findViewById(R.id.navDrawer_content);
        // setup drawer view
        setupDrawerContent(navDrawer);

        // set the image view for profile picture
        NavigationView navigationView = (NavigationView) findViewById(R.id.navDrawer_content);
        View headerLayout = navigationView.getHeaderView(0);
        ImageView profileView = (ImageView) headerLayout.findViewById(R.id.profilePicture);

        // set profile picture from shared prefrences
        SharedPreferences myPrefrence = PreferenceManager.getDefaultSharedPreferences(this);
        String imageEncoded = myPrefrence.getString("imagePreferance",null);
        if(imageEncoded != null && !imageEncoded.isEmpty()) {
            Bitmap imageBitmap = decodeBase64(imageEncoded);
            profileView.setImageBitmap(imageBitmap);
        }


        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Request the permission
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS);

        } else {
            // Permission has already been granted
            // get all contacts
            allContacts = getContacts();
        }
    }

    private ArrayList<Contact> getContacts() {

        ArrayList<Contact> contacts = new ArrayList<Contact>();

        ContentResolver contentResolver = this.getContentResolver();
        Cursor contactsContract = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

        while (contactsContract.moveToNext()) {
            String personId = contactsContract.getString(contactsContract.getColumnIndex(ContactsContract.Contacts._ID));
            String personName = contactsContract.getString(contactsContract.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

            Contact contact = new Contact(personName);
            if (contactsContract.getInt(contactsContract.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {

                String[] params = new String[] {personId};

                Cursor personCursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", params, null);

                if (personCursor.moveToNext()) {
                    contact.setPhone_number(personCursor.getString(personCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
                }

                personCursor.close();

                Cursor emailCursor = contentResolver.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ? ", params, null);

                if (emailCursor.moveToNext()) {
                    contact.setEmail(emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA)));
                }
                emailCursor.close();
            }
            contacts.add(contact);
        }
        contactsContract.close();
        return contacts;
    }

    // is invoked when the user responds to the app's permission request
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    allContacts = getContacts();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    // fragments call this method when they need the contacts data
    public ArrayList<Contact> returnContacts() {return allContacts;}

    // changes the profile picture
    public void takeANewPicture(View view) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            ImageView profileView = (ImageView) findViewById(R.id.profilePicture);
            profileView.setImageBitmap(imageBitmap);

            // store to shared preferences
            SharedPreferences myPrefrence = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = myPrefrence.edit();
            editor.putString("imagePreferance", encodeTobase64(imageBitmap));
            editor.commit();
        }
    }

    // method for bitmap to base64
    public static String encodeTobase64(Bitmap image) {
        Bitmap immage = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immage.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        Log.d("Image Log:", imageEncoded);
        return imageEncoded;
    }

    // method for base64 to bitmap
    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory
                .decodeByteArray(decodedByte, 0, decodedByte.length);
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.drawer_open, R.string.drawer_close);
    }

    // open and close the navigation drawer with the hamburger button
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // synchronize the state whenever the screen is restored or there is a configuration change
    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawerToggle.onConfigurationChanged(newConfig);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                }
        );
    }

    private void selectDrawerItem(MenuItem menuItem) {
        // create fragments based on the ones selected
        Fragment fragment = null;
        Class fragmentClass;

        switch(menuItem.getItemId()) {
            case R.id.item_home:
                fragmentClass = HomeFragment.class;
                break;
            case R.id.item_contacts:
                fragmentClass = ContactsFragment.class;
                break;
            case R.id.item_message:
                fragmentClass = MessageFragment.class;
                break;
            default:
                fragmentClass = HomeFragment.class;
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // exchange the fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_content, fragment).commit();

        // highlight selected item
        menuItem.setCheckable(true);

        // set the action bar title
        setTitle(menuItem.getTitle());

        // close navigation drawer
        mDrawer.closeDrawers();
    }
}
