package si.uni_lj.fri.pbd.miniapp1;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;

public class MessageFragment extends Fragment {

    private View view;
    private ArrayList<Contact> allContacts;
    private boolean noContactsPermission = true;
    private boolean noContactsSelected = true;
    private boolean allEmailsMissing = false;
    private boolean allNumbersMissing = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_message, container, false);

        // Listen for Send Email button press
        Button emailButton = (Button) view.findViewById(R.id.button_send_email);
        emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!noContactsPermission) {
                    if (!noContactsSelected) {
                        String uriText = "mailto:" + parseEmails() +
                                "?subject=" + getString(R.string.email_subject) +
                                "&body=" + getString(R.string.email_body);
                        Uri uri = Uri.parse(uriText);

                        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                        emailIntent.setData(uri);

                        if (!allEmailsMissing) {
                            try {
                                startActivity(Intent.createChooser(emailIntent, "Send email..."));
                            } catch (android.content.ActivityNotFoundException ex) {
                                showToast(getString(R.string.erros_no_app_found), Toast.LENGTH_SHORT);
                            }
                        }
                    } else {
                        showToast(getString(R.string.erros_no_selected), Toast.LENGTH_SHORT);
                    }
                } else {
                    showToast(getString(R.string.erros_no_users2), Toast.LENGTH_SHORT);
                }
            }
        });

        // Listen for Send MMS button press
        Button mmsButton = (Button) view.findViewById(R.id.button_send_mms);
        mmsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!noContactsPermission) {
                    if (!noContactsSelected) {
                        Intent mmsIntent = new Intent(Intent.ACTION_SENDTO);
                        mmsIntent.setData(Uri.parse("smsto:" + parseNumbers()));
                        mmsIntent.putExtra("sms_body", getString(R.string.email_body));
                        if (!allNumbersMissing) {
                            try {
                                startActivity(Intent.createChooser(mmsIntent, "Send MMS..."));
                            } catch (android.content.ActivityNotFoundException ex) {
                                showToast(getString(R.string.erros_no_app_found), Toast.LENGTH_SHORT);
                            }
                        }
                    } else {
                        showToast(getString(R.string.erros_no_selected), Toast.LENGTH_SHORT);
                    }
                } else {
                    showToast(getString(R.string.erros_no_users2), Toast.LENGTH_SHORT);
                }
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // reqest contacts from main activity
        MainActivity mainActivity = (MainActivity) getActivity();
        allContacts = mainActivity.returnContacts();

        if(allContacts != null && allContacts.size() > 0) {
            noContactsPermission = false;
            noContactsSelected = checkIfNoSelected();
        }
    }

    // shows toast with given message
    public void showToast(String message, int len) {
        Toast toast = Toast.makeText(getContext(), message, len);
        toast.show();
    }

    //checks if any contacts were selected
    public boolean checkIfNoSelected() {
        int countSelected = 0;
        for(int i=0; i<allContacts.size(); i++)
            if(allContacts.get(i).isChecked())
                countSelected++;
        return countSelected==0;
    }

    // concatenates the array of phone numbers into a single string
    public String parseNumbers() {
        String [] selectedPhoneNumbers = extractNumbers();

        // remove null entries
        ArrayList<String> list = new ArrayList<String>();
        for (String s : selectedPhoneNumbers)
            if(s != null && s.length() > 0)
                list.add(s);

        selectedPhoneNumbers = list.toArray(new String[list.size()]);

        // concatenate all phone numbers into one String and return it
        return TextUtils.join(";", selectedPhoneNumbers);
    }

    // concatenates the array of emails into a single string
    public String parseEmails() {
        // get array of emails of selected contacts
        String [] selectedEmails = extractEmails();

        // remove null entries
        ArrayList<String> list = new ArrayList<String>();
        for (String s : selectedEmails)
            if(s != null && s.length() > 0)
                list.add(s);

        selectedEmails = list.toArray(new String[list.size()]);

        // concatenate all emails into one String and return it
        return TextUtils.join(",", selectedEmails);
    }

    // return an array of emails for the selected contacts
    public String[] extractEmails() {
        int count = 0;
        int countSelected = 0;
        String[] onlyEmails = new String[allContacts.size()];

        for (int i = 0; i < allContacts.size(); i++) {
            // check if the contact was selected
            if (allContacts.get(i).isChecked()) {
                countSelected++;
                // check if selected contact has an email
                if (allContacts.get(i).getEmail() != null && !allContacts.get(i).getEmail().isEmpty()) {
                    onlyEmails[i] = allContacts.get(i).getEmail();
                } else {
                    // count how many selected contacts dont have an email
                    count++;
                }
            }
        }

        // warn user that some or all contacts dont have an email
        // some of the selected contacts dont have an email
        if(count > 0 && count != countSelected) {
            showToast(getString(R.string.erros_empty_email), Toast.LENGTH_LONG);
        }
        // all of the selected contacts dont have an email
        else if(count == countSelected) {
            allEmailsMissing = true;
            showToast(getString(R.string.erros_no_emails), Toast.LENGTH_LONG);
        }
        return onlyEmails;
    }

    // return an array of phone numbers for the selected contacts
    public String[] extractNumbers() {
        int count = 0;
        int countSelected = 0;
        String[] onlyNumbers = new String[allContacts.size()];

        for (int i = 0; i < allContacts.size(); i++) {
            if (allContacts.get(i).isChecked()) {
                countSelected++;
                if(allContacts.get(i).getPhone_number() != null && !allContacts.get(i).getPhone_number().isEmpty()) {
                    onlyNumbers[i] = allContacts.get(i).getPhone_number();
                } else {
                    count ++;
                }
            }
        }

        // at least one, but not all selected contacts dont have a phone number
        if(count > 0 && count != countSelected) {
            showToast(getString(R.string.erros_empty_number), Toast.LENGTH_LONG);
        }
        // all selected contacts dont have a phone number
        else if(count == countSelected) {
            allNumbersMissing = true;
            showToast(getString(R.string.erros_no_numbers), Toast.LENGTH_LONG);
        }
        return onlyNumbers;
    }
}
